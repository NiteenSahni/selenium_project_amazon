package org.example;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.twilio.Twilio;
import net.bytebuddy.pool.TypePool;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static java.lang.Integer.parseInt;
import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfWindowsToBe;
import com.twilio.Twilio;
import com.twilio.rest.chat.v2.service.channel.Message;
import com.twilio.type.PhoneNumber;

public class Main {
//    static ChromeDriver driver;
    static WebDriver driver;
    static JavascriptExecutor js;
    public static ExtentReports report = new ExtentReports(System.getProperty("user.dir") + "ExtentReportResults.html");
    public static ExtentTest test = report.startTest("ExtentReport");
    public static void main(String[] args) throws InterruptedException {

//      Invoking the browser and opening the amazon page using the get() method.

        driver  = selectBrowser("chrome");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        driver.get("https://www.amazon.in/");
        Thread.sleep(10000);

//
//        userRegistration();
//        userLogin();
//        mouseHoverOverSlideMenu();
//        scrollUsingActionClass();
//        searchProduct();
//        addToCart();
        buyProduct();

    }
   public static WebDriver selectBrowser(String browserName){

        if(browserName.equalsIgnoreCase("Chrome")){
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver();
        }
       if(browserName.equalsIgnoreCase("FireFox")){
           WebDriverManager.firefoxdriver().setup();
           return  new FirefoxDriver();
       }

       return null;
   }


    @BeforeTest
    public static void invokeBrowser() {
        WebDriverManager.chromedriver().setup();
//        driver = new ChromeDriver();
        driver =  selectBrowser("chrome");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        driver.get("https://www.amazon.in/");


    }

    //function automates the user registration process, and fills the required fields.
    @Test (priority = 0)
    public static void userRegistration() throws InterruptedException {
        try {

            driver.findElement(By.id("nav-link-accountList-nav-line-1")).click();
            driver.findElement(By.id("createAccountSubmit")).click();
            driver.findElement(By.id("ap_customer_name")).sendKeys("Niteen Sahni");
            driver.findElement(By.id("ap_phone_number")).sendKeys("772105256");
            driver.findElement(By.id("ap_password")).sendKeys("Nks007786#");
            Thread.sleep(2000);
        } catch (Exception e){
            System.out.println("Element not found, FunctionName");
            System.out.println(e);
        }

    }

   // function automates the login process of amazon.in filling in the necessary fields.
    @Test (priority = 1)
    public static void userLogin() throws InterruptedException {
        try {
            driver.findElement(By.partialLinkText("Sign in")).click();
            driver.findElement(By.id("ap_email")).sendKeys("7721095256");
            driver.findElement(By.id("continue")).click();
            driver.findElement(By.id("ap_password")).sendKeys("Nks007786#");
            driver.findElement(By.id("signInSubmit")).click();

        } catch (Exception e){
            System.out.println("Element not found");
            System.out.println(e);
        }
    }

    // function replicates the mouse hover effect using the Actions class and moveToElement method.
    @Test (priority = 2)
    public static void mouseHoverOverSlideMenu() throws InterruptedException {
        Actions action = new Actions(driver);
        try {
            driver.findElement(By.id("nav-hamburger-menu")).click();
            Thread.sleep(2000);
            WebElement element = (driver.findElement(By.xpath("//*[@id=\"hmenu-content\"]/ul[1]/li[2]/a")));
            action.moveToElement(element).click().perform();
        } catch (Exception e){
            System.out.println("Element not found");
            System.out.println(e);
        }

    }

    // function replicates the mouse scroll effect using the Actions class nad scrollToElement method.
    @Test (priority = 3)
    public static void scrollUsingActionClass() throws InterruptedException {
        Actions action = new Actions(driver);
        try {
            WebElement targetElement = driver.findElement(By.id("navBackToTop"));
            action.scrollToElement(targetElement).perform();
        } catch (Exception e){
            System.out.println("Element not found");
            System.out.println(e);
        }
    }

    // function automates the process of searching for a product.
    @Test (priority = 4)
    public static void searchProduct() throws InterruptedException {
       try {
           WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
           searchBox.sendKeys("god of war");
           WebElement search = driver.findElement(By.id("nav-search-submit-button"));
           search.click();
           Thread.sleep(3000);
       } catch (Exception e){
           System.out.println("Element not found");
           System.out.println(e);
       }

    }

    // function replicates the process of adding an element to the cart.
    @Test (priority = 5)
    public static void addToCart() throws InterruptedException {
     try {
         userLogin();
         WebElement cartElement = driver.findElement(By.id("nav-cart-count"));
         String cartTotal = cartElement.getAttribute("innerHTML");
         System.out.println(cartTotal);
         driver.findElement(By.id("nav-cart")).click();
         driver.findElement(By.className("a-button-input")).click();
     } catch (Exception e){
         System.out.println("Element not found");
         System.out.println(e);
     }
    }

    // function replicates the process of searching for a product, selecting the product and clicking buynow and moving to the checkOut page.
    @Test
    public static void buyProduct() throws InterruptedException {
//        invokeBrowser();
        searchProduct();
        try {
            String originalWindow = driver.getWindowHandle();
            assert driver.getWindowHandles().size() == 1;
            driver.findElement(By.className("s-image")).click();
            for (String windowHandle : driver.getWindowHandles()) {
                if (!originalWindow.contentEquals(windowHandle)) {
                    driver.switchTo().window(windowHandle);
                    break;
                }
            }
            driver.findElement(By.id("add-to-cart-button")).click();
            driver.findElement(By.name("proceedToRetailCheckout")).click();
            driver.findElement(By.id("ap_email")).sendKeys("7721095256");
            driver.findElement(By.id("continue")).click();
            driver.findElement(By.id("ap_password")).sendKeys("Nks007786#");
            driver.findElement(By.id("signInSubmit")).click();
            driver.findElement(By.xpath("//*[@id=\"orderSummaryPrimaryActionBtn\"]/span/input")).click();
        } catch (Exception e){
            System.out.println("Element not found");
            System.out.println(e);
        }


    }


}
